/*
  ==============================================================================

   This file is part of the JUCE tutorials.
   Copyright (c) 2017 - ROLI Ltd.

   The code included in this file is provided under the terms of the ISC license
   http://www.isc.org/downloads/software-support-policy/isc-license. Permission
   To use, copy, modify, and/or distribute this software for any purpose with or
   without fee is hereby granted provided that the above copyright notice and
   this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY, AND ALL WARRANTIES,
   WHETHER EXPRESSED OR IMPLIED, INCLUDING MERCHANTABILITY AND FITNESS FOR
   PURPOSE, ARE DISCLAIMED.

  ==============================================================================
*/

/*******************************************************************************
 The block below describes the properties of this PIP. A PIP is a short snippet
 of code that can be read by the Projucer and used to generate a JUCE project.

 BEGIN_JUCE_PIP_METADATA

 name:             PlayingSoundFilesTutorial
 version:          1.0.0
 vendor:           JUCE
 website:          http://juce.com
 description:      Plays audio files.

 dependencies:     juce_audio_basics, juce_audio_devices, juce_audio_formats,
                   juce_audio_processors, juce_audio_utils, juce_core,
                   juce_data_structures, juce_events, juce_graphics,
                   juce_gui_basics, juce_gui_extra
 exporters:        xcode_mac, vs2017, linux_make

 type:             Component
 mainClass:        MainContentComponent

 useLocalCopy:     1

 END_JUCE_PIP_METADATA

*******************************************************************************/

#pragma once

#include "Filter.hpp"
#include "Delay.hpp"
#include "Filters/filters.h"
#include "Effects/Wah.hpp"
#include "Effects/Distortion.hpp"

//==============================================================================
class MainContentComponent   : public AudioAppComponent,
                               public ChangeListener
{
public:
    MainContentComponent()
        :   state (Stopped)
    {
        addAndMakeVisible (&openButton);
        openButton.setButtonText ("Open...");
        openButton.onClick = [this] { openButtonClicked(); };

        addAndMakeVisible (&playButton);
        playButton.setButtonText ("Play");
        playButton.onClick = [this] { playButtonClicked(); };
        playButton.setColour (TextButton::buttonColourId, Colours::green);
        playButton.setEnabled (false);

        addAndMakeVisible (&stopButton);
        stopButton.setButtonText ("Stop");
        stopButton.onClick = [this] { stopButtonClicked(); };
        stopButton.setColour (TextButton::buttonColourId, Colours::red);
        stopButton.setEnabled (false);

        addAndMakeVisible(&levelSlider);
        levelSlider.setRange(0.0, 1.0);
        levelSlider.onValueChange = [this] {levelSliderValueChanged();};
        
        addAndMakeVisible(&delaySlider);
        delaySlider.setRange(0.0,1.0);
        delaySlider.onValueChange = [this] {delaySliderValueChanged(); };
        
        addAndMakeVisible(&filterIO);
        filterIO.setButtonText("Filter I/O");
        filterIO.onClick = [this] {filterButtonClicked();};
        filterBypassed = true;
        
        setSize (300, 200);

        formatManager.registerBasicFormats();       // [1]
        transportSource.addChangeListener (this);   // [2]

        setAudioChannels (0, 2);
        
        for (int chan = 0; chan < 2; chan++)
        {
            dist[chan].makeQuarterCircle();
        }
    
    }

    ~MainContentComponent()
    {
        shutdownAudio();
    }

    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override
    {
        transportSource.prepareToPlay (samplesPerBlockExpected, sampleRate);
    }

    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
    {
        
//        auto level = levelSlider.getValue();
        
        if (readerSource.get() == nullptr)
        {
            bufferToFill.clearActiveBufferRegion();
            return;
        }
        else{
            transportSource.getNextAudioBlock (bufferToFill);
        
            for (int chan = 0; chan < bufferToFill.buffer->getNumChannels(); chan++)
            {
            
                auto* writePointer = bufferToFill.buffer->getWritePointer(chan);
                auto* readPointer = bufferToFill.buffer->getReadPointer(chan);
                int bufferSize = bufferToFill.buffer->getNumSamples();
                
                dist[chan].processBlock(writePointer, bufferSize);
                
                
//                biquad[chan].processBlock(writePointer,bufferSize);


//                for(int sample = bufferToFill.startSample; sample < bufferSize; sample++) //PROCESS AUDIO WITH EFFECTS HERE
//                {
//                    if(!filterBypassed)
////                        writePointer += delay.getSample (writePointer);
//                        myDelay[chan].processSample(readPointer, writePointer, sample);
//
////                    *writePointer = myDelay[chan].process(*writePointer);
////                    writePointer++;
////                    writePointer[sample] *= level;
//                }
            }
        }
    }

    void releaseResources() override
    {
        transportSource.releaseResources();
    }

    void resized() override
    {
        openButton.setBounds (10, 10, getWidth() - 20, 20);
        playButton.setBounds (10, 40, getWidth() - 20, 20);
        stopButton.setBounds (10, 70, getWidth() - 20, 20);
        levelSlider.setBounds(10, 100, getWidth() - 20, 20);
        delaySlider.setBounds(10,130, getWidth() - 20, 20);
        filterIO.setBounds(10, 160, getWidth()-20, 20);
    }

    void changeListenerCallback (ChangeBroadcaster* source) override
    {
        if (source == &transportSource)
        {
            if (transportSource.isPlaying())
                changeState (Playing);
            else
                changeState (Stopped);
        }
    }

private:
    enum TransportState
    {
        Stopped,
        Starting,
        Playing,
        Stopping
    };

    void changeState (TransportState newState)
    {
        if (state != newState)
        {
            state = newState;

            switch (state)
            {
                case Stopped:                           // [3]
                    stopButton.setEnabled (false);
                    playButton.setEnabled (true);
                    transportSource.setPosition (0.0);
                    break;

                case Starting:                          // [4]
                    playButton.setEnabled (false);
                    transportSource.start();
                    break;

                case Playing:                           // [5]
                    stopButton.setEnabled (true);
                    break;

                case Stopping:                          // [6]
                    transportSource.stop();
                    break;
            }
        }
    }

    void openButtonClicked()
    {
        FileChooser chooser ("Select a Wave file to play...",
                             File::nonexistent,
                             "*.wav");                                        // [7]

        if (chooser.browseForFileToOpen())                                    // [8]
        {
            auto file = chooser.getResult();                                  // [9]
            auto* reader = formatManager.createReaderFor (file);              // [10]

            if (reader != nullptr)
            {
                std::unique_ptr<AudioFormatReaderSource> newSource (new AudioFormatReaderSource (reader, true)); // [11]
                transportSource.setSource (newSource.get(), 0, nullptr, reader->sampleRate);                     // [12]
                playButton.setEnabled (true);                                                                    // [13]
                readerSource.reset (newSource.release());                                                        // [14]
            }
        }
    }

    void playButtonClicked()
    {
        changeState (Starting);
    }

    void stopButtonClicked()
    {
        changeState (Stopping);
    }
    
    void filterButtonClicked()
    {
        if(filterBypassed)
        {
            filterBypassed = false;
        }
        else
        {
            filterBypassed = true;
        }
    }
    
    void delaySliderValueChanged()
    {

        for (int chan = 0; chan < 2; chan++)
        {
            dist[chan].setGain((float)delaySlider.getValue()*30.f);
        }
    }
    void levelSliderValueChanged()
    {
        for (int chan = 0; chan < 2; chan++)
        {
            dist[chan].setAttenuation((float)levelSlider.getValue());
        }
    }

    //==========================================================================
    TextButton openButton;
    TextButton playButton;
    TextButton stopButton;
    
    Slider levelSlider;
    Slider delaySlider;
    
    TextButton filterIO;
    double xm1 = 0;
    
    MyFilter filterBro;
    
    WetBoy::Biquad<float> biquad[2];
    WetBoy::OnePole<float> onePole[2];
    
    Delay myDelay[2];
    WetBoy::Wah<float> wah[2];
    WetBoy::Distortion<float> dist[2];
    
    bool filterBypassed = true;

    AudioFormatManager formatManager;
    std::unique_ptr<AudioFormatReaderSource> readerSource;
    AudioTransportSource transportSource;
    TransportState state;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};
