//
//  Filter.cpp
//  PlayingSoundFilesTutorial - App
//
//  Created by Tom Sanger on 15/06/2018.
//  Copyright © 2018 JUCE. All rights reserved.
//

#include "Filter.hpp"

MyFilter::MyFilter()
{
    filterTaps[0] = 0.01156111;
    filterTaps[1] = 0.05773286;
    filterTaps[2] = 0.12773940;
    filterTaps[3] = 0.19304879;
    filterTaps[4] = 0.21983567;
    filterTaps[5] = 0.19304879;
    filterTaps[6] = 0.12773940;
    filterTaps[7] = 0.05773286;
    filterTaps[8] = 0.01156111;
    
}
float MyFilter::processAudio(const float* x,float* y, int bufferSize, float xm1)
{
    int n;
    
    y[0] = x[0] + xm1;
    
    for(n=1; n < bufferSize; n++){
        y[n] += (x[n] + x[n-1])/5;
    }
    return x[bufferSize-1];
}

void MyFilter::processAudio2(const float* x,float* y, int bufferSize, float xm1)
{
    float output = 0;
    
    for(int sample = 0; sample < bufferSize; sample++)
    {
        for(int i = 8; i >= 0; i--)
        {
            delayArray[i+1] = delayArray[i];
        }
        
        delayArray[0] = x[sample];
        
        for (int i = 0; i<9; i++) {
            output = output + delayArray[i] * filterTaps [i];
            }
        
        y[sample] = output;
    }
}
