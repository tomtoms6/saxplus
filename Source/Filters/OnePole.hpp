//
//  OnePoleLowPass.hpp
//  PlayingSoundFilesTutorial - App
//
//  Created by Dominic Brown on 15/06/2018.
//  Copyright © 2018 JUCE. All rights reserved.
//

#ifndef OnePole_hpp
#define OnePole_hpp

#include <stdio.h>
#include <cmath>

namespace WetBoy
{
    template <typename Type>
    class OnePole
    {
    private:
        Type y1;
        //Filter coeffs
        Type a0, b1, c;
        Type frequency;
        int sampleRate;
        void calculateCoefficients ()
        {
            c = 2 - std::cos ((M_2_PI*frequency)/(Type)sampleRate);
            b1 = std::sqrt(c*c-1)-c;
            a0 = 1 + b1;
        }
    public:
        OnePole() : a0 (0), b1 (0), c (0), sampleRate (44100), frequency (0), y1(0) {}
        
        void setSampleRate (int sr)
        {
            sampleRate = sr;
            calculateCoefficients();
        }
        
        void setCutoffFreq (Type freq)
        {
            frequency = freq;
            calculateCoefficients();
        }
        Type process (Type x0)
        {
            Type y0 = (a0 * x0) - (b1 * y1);
            y1 = y0;
            return y0;
        }
        
        void processBlock (Type* buffer, int bufferSize)
        {
            for (int i = 0; i < bufferSize; i++)
            {
                *buffer = process(*buffer);
                buffer++;
            }
        }
    };
}

#endif /* OnePoleLowPass_hpp */
