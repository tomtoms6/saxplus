//
//  filters.h
//  PlayingSoundFilesTutorial
//
//  Created by Dominic Brown on 15/06/2018.
//  Copyright © 2018 JUCE. All rights reserved.
//

#ifndef filters_h
#define filters_h

/**Header file for WetBoy filters*/


#include "OnePole.hpp"
#include "Biquad.hpp"

#endif /* filters_h */
