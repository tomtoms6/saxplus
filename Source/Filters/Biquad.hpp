//
//  Biquad.hpp
//  PlayingSoundFilesTutorial
//
//  Created by Dominic Brown on 16/06/2018.
//

#ifndef Biquad_h
#define Biquad_h

#include <stdio.h>
#include <functional>
#include <cmath>

namespace WetBoy
{
    template <typename Type>
    class Biquad
    {
    private:
        Type x1, x2, y1, y2;
        Type a0, a1, a2, b1, b2;
        Type q, f;
        int sr;
        std::function<void()> calculateCoefficients;
        
    public:
        
        Biquad() : x1(0), x2(0), y1(0), y2(0), a0(0), a1(0), b1(0), b2(0), sr(44100), f(0), q(0.7071)
        {
            makeLowPass();
        }
        
        void makeLowPass()
        {
            calculateCoefficients = [this]()
            {
                //low pass coefficients here
                Type phi = (M_2_PI * f)/(Type)sr;
                
                b2 = (2.0 * q - std::sin(phi))/(2.0 * q + std::sin(phi));
                b1 = -(1.0 + b2) * std::cos(phi);
                
                a0 = 0.25 * (1.0 + b1 + b2);
                a1 = 2.0 * a0;
                a2 = a0;
                
            };
            
            calculateCoefficients();
        }
        void makeHighPass()
        {
            calculateCoefficients = [this]()
            {
                //high pass coeffs
                Type phi = (M_2_PI * f)/(Type)sr;
                
                b2 = (2.0 * q - std::sin(phi))/(2.0 * q + std::sin(phi));
                b1 = -(1.0 + b2) * std::cos(phi);
                
                a0 = 0.25 * (1.0 - b1 + b2);
                a1 = -2.0 * a0;
                a2 = a0;
            };
            
            calculateCoefficients();
        }
        
        void makeBandPass()
        {
            calculateCoefficients = [this]()
            {
                if(f < 0)
                    f = 0;
                else if(f > (q*sr)/4.0)
                    f = (q*sr)/4.0;
                
                Type phi = (M_2_PI * f)/(Type)sr;
                
                b2 = std::tan (M_PI / 4.0 - phi / (2 * q));
                
                b1 = -(1 + b2) * std::cos (phi);
                
                a0 = 0.5 * (1.0 - b2);
                a1 = 0.0;
                a2 = -a0;
            };
            
            calculateCoefficients();
        }
        void makeBandReject()
        {
            calculateCoefficients = [this]()
            {
                if(f < 0)
                    f = 0;
                else if(f < (q*sr)/4.0)
                    f = (q*sr)/4.0;
                
                Type phi = (M_2_PI * f)/(Type)sr;
                
                b2 = std::tan (M_PI / 4.0 - phi / (2 * q));
                
                b1 = -(1 + b2) * std::cos (phi);
                
                a0 = 0.5 * (1.0 + b2);
                a1 = b1;
                a2 = a0;
            };
            
            calculateCoefficients();
        }
        
        void setSampleRate (const int sampleRate)
        {
            sr = sampleRate;
            
            calculateCoefficients();
        }
        
        void setFrequency (const Type frequency)
        {
            if (frequency > sr/2)
                f = sr/2;
            else
                f = frequency;
            
            calculateCoefficients();
        }
        void setQ (const Type q_)
        {
            if (q_ < 0.7071) //sqrt 0.5
                q = 0.7071;
            else
                q = q_;
            
            calculateCoefficients();
        }
        
        Type process (const Type x0)
        {
            Type y0 = (a0*x0) + (a1*x1) + (a2*x2) - (b1*y1) - (b2*y2);
            x2 = x1;
            x1 = x0;
            y2 = y1;
            y1 = y0;
            return y0;
        }
        
        void processBlock(Type* buffer, int bufferSize)
        {
            for(int i = 0; i < bufferSize; i++)
            {
                *buffer = process(*buffer);
                buffer++;
            }
        }
    };
}
#endif /* Biquad_h */
