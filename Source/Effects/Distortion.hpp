//
//  Distortion.hpp
//  PlayingSoundFilesTutorial - App
//
//  Created by Dominic Brown on 22/06/2018.
//

#ifndef Distortion_hpp
#define Distortion_hpp

#include <stdio.h>
#include <cmath>
#include <functional>


namespace WetBoy
{
    template <typename Type>
    class Distortion
    {
    private:
        Type a, gain;
        
        std::function<Type(const Type)> shape;
        std::function<void(Type&)> attenuation;
        std::function<Type(const Type)> clip;
        
        Type hardClip (const Type input)
        {
            if (input > 1.0)
                return 1.0;
            else if (input < -1.0)
                return -1.0;
            else
                return input;
        }
        
    public:
        Distortion() : a(2), gain (1)
        {
            
            makeSoftClip();
        }
        
        void makeSoftClip ()
        {
            clip = [this] (const Type input) {return hardClip (input);};
            
            shape = [this] (const Type input) {
                
                Type out;
                if (input > 0)
                    out = 1-std::pow(a,-input);
                else
                    out = -1+std::pow(a, input);
                
                return out*a/(a-1);
                
            };
            
            attenuation = [this] (Type& input)
            {
                input = input * 998 + 2;
            };
        }
        
        
        Type reflect (Type input)
        {
            Type output;
            if (input > 1.0)
                output = 1.0-input;
            else if (input < -1.0)
                output = -1.0-input;
            else
                output = input;
            
            if (abs(output) > 1.0)
                reflect(output);
            else
                return output;
        }
        
        void makeReflector ()
        {
            
            clip = [this] (const Type input)
            {
                return reflect(input);
            };
            shape = [this] (const Type input)
            {
                return input;
            };
        }
        
        void makeRectification ()
        {
            clip = [this] (const Type input) {return hardClip (input);};
            
            shape = [this] (const Type input)
            {
                Type out;
                if (input < 0)
                    out = -input;
                else
                    out = input;
                
                return out;
            };
            attenuation = [this] (Type& input) {};
        }
        
        void makeQuantisation()
        {
            clip = [this] (const Type input) {return hardClip (input);};
            
            shape = [this] (const Type input)
            {
                Type out;
                out = input * a;
                out = (int)out;
                out = (Type)out/a;
                return out;
            };
            attenuation = [this] (Type& input)
            {
                input *= 30;
                input += 3;
            };
        }
        
        void makeAsymmetrical() //Needs to be implemented 
        {
            clip = [this] (const Type input) {hardClip (input);};
            
            shape = [this] (const Type input)
            {
                Type out;
                if (input > 0)
                    out = input;
                else
                    ;
            };
            attenuation = [this] (Type& input) {};
        }
        
        void makeQuarterCircle()
        {
            clip = [this] (const Type input) {return hardClip (input);};
            
            shape = [this] (const Type input) {
                Type out;
                if (input > 0)
                    out = std::sqrt(1-(input-1)*(input-1));
                else
                    out = -std::sqrt(1-(input+1)*(input+1));
                return out;
            };
            
            attenuation = [this] (Type& input) {};
        }
        void setGain(const Type gain_)
        {
            gain = gain_;
        }
        void setAttenuation (const Type a_)
        {
            a = a_;
            attenuation(a);
        }
        Type process (const Type input)
        {
            Type dist = input * gain;
            return shape(clip(dist));
        }
        void processBlock(Type* buffer, int bufferSize)
        {
            for (int i = 0; i < bufferSize; i++)
            {
                *buffer = process(*buffer);
                buffer++;
            }
        }
    };
}

#endif /* Distortion_hpp */
