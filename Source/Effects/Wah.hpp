//
//  Wah.hpp
//  PlayingSoundFilesTutorial - App
//
//  Created by Dominic Brown on 22/06/2018.
//

#ifndef Wah_hpp
#define Wah_hpp

#include <stdio.h>
#include "../Filters/filters.h"

/**
 Implementation of simple Wah effect
 */
namespace WetBoy
{
    template <typename Type>
    class Wah
    {
    private:
//        Type control;
        Type f1, f2, q;
        Type a1 /*wet*/, a2 /*dry*/;
        WetBoy::Biquad<Type> bandpass;
    public:
        Wah() : f1(5000.0), f2(200.0), q(6.0), a1(0.5), a2(0.5)
        {
            bandpass.makeBandPass();
            bandpass.setQ(6.0);
        }
        void setWah(Type wahAmount)
        {
            Type control = wahAmount*wahAmount*wahAmount;
            Type freq = control * f1 + f2;
            bandpass.setFrequency(freq);
        }
        Type process (const Type in)
        {
            return bandpass.process(in);
        }
        void processBlock (Type* buffer, int bufferSize)
        {
            bandpass.processBlock(buffer, bufferSize);
        }
    };
}



#endif /* Wah_hpp */
