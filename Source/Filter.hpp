//
//  Filter.hpp
//  PlayingSoundFilesTutorial - App
//
//  Created by Tom Sanger on 15/06/2018.
//  Copyright © 2018 JUCE. All rights reserved.
//

#ifndef Filter_hpp
#define Filter_hpp

#include <stdio.h>

#endif /* Filter_hpp */


class MyFilter{
    
public:
    MyFilter();
    float processAudio(const float* x,float* y, int bufferSize, float);
    void processAudio2(const float* x,float* y, int bufferSize, float);
    
    float filterTaps[9];
    float delayArray[9];
};
