Add a symbolic link to the JUCE library here.

- Open Terminal

- Use the cd command to change the directory to this folder, i.e. cd /Directory/to/SaxPlus++/Libs

- add a symbolic link using ln -s /Directory/to/JUCE